package com.cskaoyan.mapper;

import com.cskaoyan.bean.model.MarketCoupon;
import com.cskaoyan.bean.model.MarketCouponExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

/**
*@Description：
*@Author：BLUEBLANK
*@Date：2024/4/11  20:05
*/


    
public interface MarketCouponMapper {
    long countByExample(MarketCouponExample example);

    int deleteByExample(MarketCouponExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(MarketCoupon record);

    int insertSelective(MarketCoupon record);

    List<MarketCoupon> selectByExample(MarketCouponExample example);

    MarketCoupon selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") MarketCoupon record, @Param("example") MarketCouponExample example);

    int updateByExample(@Param("record") MarketCoupon record, @Param("example") MarketCouponExample example);

    int updateByPrimaryKeySelective(MarketCoupon record);

    int updateByPrimaryKey(MarketCoupon record);
}