package com.cskaoyan.mapper;

import com.cskaoyan.bean.model.MarketCouponUser;
import com.cskaoyan.bean.model.MarketCouponUserExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

/**
*@Description：
*@Author：BLUEBLANK
*@Date：2024/4/11  20:05
*/


    
public interface MarketCouponUserMapper {
    long countByExample(MarketCouponUserExample example);

    int deleteByExample(MarketCouponUserExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(MarketCouponUser record);

    int insertSelective(MarketCouponUser record);

    List<MarketCouponUser> selectByExample(MarketCouponUserExample example);

    MarketCouponUser selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") MarketCouponUser record, @Param("example") MarketCouponUserExample example);

    int updateByExample(@Param("record") MarketCouponUser record, @Param("example") MarketCouponUserExample example);

    int updateByPrimaryKeySelective(MarketCouponUser record);

    int updateByPrimaryKey(MarketCouponUser record);
}