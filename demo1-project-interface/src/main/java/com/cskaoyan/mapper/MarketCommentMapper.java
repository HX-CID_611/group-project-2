package com.cskaoyan.mapper;

import com.cskaoyan.bean.model.MarketComment;
import com.cskaoyan.bean.model.MarketCommentExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

/**
*@Description：
*@Author：BLUEBLANK
*@Date：2024/4/11  20:05
*/


    
public interface MarketCommentMapper {
    long countByExample(MarketCommentExample example);

    int deleteByExample(MarketCommentExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(MarketComment record);

    int insertSelective(MarketComment record);

    List<MarketComment> selectByExample(MarketCommentExample example);

    MarketComment selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") MarketComment record, @Param("example") MarketCommentExample example);

    int updateByExample(@Param("record") MarketComment record, @Param("example") MarketCommentExample example);

    int updateByPrimaryKeySelective(MarketComment record);

    int updateByPrimaryKey(MarketComment record);
}