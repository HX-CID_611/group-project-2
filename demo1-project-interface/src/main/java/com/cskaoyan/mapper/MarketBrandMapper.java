package com.cskaoyan.mapper;

import com.cskaoyan.bean.model.MarketBrand;
import com.cskaoyan.bean.model.MarketBrandExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

/**
*@Description：
*@Author：BLUEBLANK
*@Date：2024/4/11  20:05
*/


    
public interface MarketBrandMapper {
    long countByExample(MarketBrandExample example);

    int deleteByExample(MarketBrandExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(MarketBrand record);

    int insertSelective(MarketBrand record);

    List<MarketBrand> selectByExample(MarketBrandExample example);

    MarketBrand selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") MarketBrand record, @Param("example") MarketBrandExample example);

    int updateByExample(@Param("record") MarketBrand record, @Param("example") MarketBrandExample example);

    int updateByPrimaryKeySelective(MarketBrand record);

    int updateByPrimaryKey(MarketBrand record);
}