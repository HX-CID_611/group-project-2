package com.cskaoyan.bean.model;

import java.util.Date;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
*@Description：
*@Author：BLUEBLANK
*@Date：2024/4/11  20:05
*/


    
/**
 * 通知表
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class MarketNotice {
    private Integer id;

    /**
    * 通知标题
    */
    private String title;

    /**
    * 通知内容
    */
    private String content;

    /**
    * 创建通知的管理员ID，如果是系统内置通知则是0.
    */
    private Integer adminId;

    /**
    * 创建时间
    */
    private Date addTime;

    /**
    * 更新时间
    */
    private Date updateTime;

    /**
    * 逻辑删除
    */
    private Boolean deleted;
}