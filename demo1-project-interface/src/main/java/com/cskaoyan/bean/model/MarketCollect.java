package com.cskaoyan.bean.model;

import java.util.Date;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
*@Description：
*@Author：BLUEBLANK
*@Date：2024/4/11  20:05
*/


    
/**
 * 收藏表
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class MarketCollect {
    private Integer id;

    /**
    * 用户表的用户ID
    */
    private Integer userId;

    /**
    * 如果type=0，则是商品ID；如果type=1，则是专题ID
    */
    private Integer valueId;

    /**
    * 收藏类型，如果type=0，则是商品ID；如果type=1，则是专题ID
    */
    private Byte type;

    /**
    * 创建时间
    */
    private Date addTime;

    /**
    * 更新时间
    */
    private Date updateTime;

    /**
    * 逻辑删除
    */
    private Boolean deleted;
}