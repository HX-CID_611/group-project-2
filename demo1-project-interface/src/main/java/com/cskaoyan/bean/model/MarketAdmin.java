package com.cskaoyan.bean.model;

import java.util.Date;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
*@Description：
*@Author：BLUEBLANK
*@Date：2024/4/11  20:05
*/


    
/**
 * 管理员表
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class MarketAdmin {
    private Integer id;

    /**
    * 管理员名称
    */
    private String username;

    /**
    * 管理员密码
    */
    private String password;

    /**
    * 最近一次登录IP地址
    */
    private String lastLoginIp;

    /**
    * 最近一次登录时间
    */
    private Date lastLoginTime;

    /**
    * 头像图片
    */
    private String avatar;

    /**
    * 创建时间
    */
    private Date addTime;

    /**
    * 更新时间
    */
    private Date updateTime;

    /**
    * 逻辑删除
    */
    private Boolean deleted;

    /**
    * 角色列表
    */
    private String roleIds;
}