package com.cskaoyan.bean.model;

import java.util.Date;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
*@Description：
*@Author：BLUEBLANK
*@Date：2024/4/11  20:05
*/


    
/**
 * 商品规格表
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class MarketGoodsSpecification {
    private Integer id;

    /**
    * 商品表的商品ID
    */
    private Integer goodsId;

    /**
    * 商品规格名称
    */
    private String specification;

    /**
    * 商品规格值
    */
    private String value;

    /**
    * 商品规格图片
    */
    private String picUrl;

    /**
    * 创建时间
    */
    private Date addTime;

    /**
    * 更新时间
    */
    private Date updateTime;

    /**
    * 逻辑删除
    */
    private Boolean deleted;
}