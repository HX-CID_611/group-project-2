package com.cskaoyan.bean.model;

import java.util.Date;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
*@Description：
*@Author：BLUEBLANK
*@Date：2024/4/11  20:05
*/


    
/**
 * 常见问题表
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class MarketIssue {
    private Integer id;

    /**
    * 问题标题
    */
    private String question;

    /**
    * 问题答案
    */
    private String answer;

    /**
    * 创建时间
    */
    private Date addTime;

    /**
    * 更新时间
    */
    private Date updateTime;

    /**
    * 逻辑删除
    */
    private Boolean deleted;
}