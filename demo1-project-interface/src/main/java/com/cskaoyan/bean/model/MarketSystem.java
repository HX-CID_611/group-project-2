package com.cskaoyan.bean.model;

import java.util.Date;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
*@Description：
*@Author：BLUEBLANK
*@Date：2024/4/11  20:05
*/


    
/**
 * 系统配置表
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class MarketSystem {
    private Integer id;

    /**
    * 系统配置名
    */
    private String keyName;

    /**
    * 系统配置值
    */
    private String keyValue;

    /**
    * 创建时间
    */
    private Date addTime;

    /**
    * 更新时间
    */
    private Date updateTime;

    /**
    * 逻辑删除
    */
    private Boolean deleted;
}