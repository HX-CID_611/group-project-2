package com.cskaoyan.bean.model;

import java.util.Date;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
*@Description：
*@Author：BLUEBLANK
*@Date：2024/4/11  20:05
*/


    
/**
 * 权限表
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class MarketPermission {
    private Integer id;

    /**
    * 角色ID
    */
    private Integer roleId;

    /**
    * 权限
    */
    private String permission;

    /**
    * 创建时间
    */
    private Date addTime;

    /**
    * 更新时间
    */
    private Date updateTime;

    /**
    * 逻辑删除
    */
    private Boolean deleted;
}