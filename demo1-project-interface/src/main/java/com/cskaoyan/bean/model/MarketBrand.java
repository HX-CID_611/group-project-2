package com.cskaoyan.bean.model;

import java.math.BigDecimal;
import java.util.Date;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
*@Description：
*@Author：BLUEBLANK
*@Date：2024/4/11  20:05
*/


    
/**
 * 品牌商表
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class MarketBrand {
    private Integer id;

    /**
    * 品牌商名称
    */
    private String name;

    /**
    * 品牌商简介
    */
    private String desc;

    /**
    * 品牌商页的品牌商图片
    */
    private String picUrl;

    private Byte sortOrder;

    /**
    * 品牌商的商品低价，仅用于页面展示
    */
    private BigDecimal floorPrice;

    /**
    * 创建时间
    */
    private Date addTime;

    /**
    * 更新时间
    */
    private Date updateTime;

    /**
    * 逻辑删除
    */
    private Boolean deleted;
}