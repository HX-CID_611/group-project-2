package com.cskaoyan.bean.model;

import java.math.BigDecimal;
import java.util.Date;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
*@Description：
*@Author：BLUEBLANK
*@Date：2024/4/11  20:05
*/


    
/**
 * 商品货品表
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class MarketGoodsProduct {
    private Integer id;

    /**
    * 商品表的商品ID
    */
    private Integer goodsId;

    /**
    * 商品规格值列表，采用JSON数组格式
    */
    private String specifications;

    /**
    * 商品货品价格
    */
    private BigDecimal price;

    /**
    * 商品货品数量
    */
    private Integer number;

    /**
    * 商品货品图片
    */
    private String url;

    /**
    * 创建时间
    */
    private Date addTime;

    /**
    * 更新时间
    */
    private Date updateTime;

    /**
    * 逻辑删除
    */
    private Boolean deleted;
}