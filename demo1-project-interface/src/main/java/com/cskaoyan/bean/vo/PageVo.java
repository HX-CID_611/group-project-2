package com.cskaoyan.bean.vo;

import com.github.pagehelper.PageInfo;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@NoArgsConstructor
@Data
public class PageVo<T> {
    private Long total;
    private Integer pages;
    private Integer limit;
    private Integer page;
    private List<T> list;

    public static <T> PageVo<T> list(List<T> list) {
        PageVo<T> pageVo = new PageVo<>();
        PageInfo<T> pageInfo = new PageInfo<>(list);
        pageVo.setPage(pageInfo.getPageNum());
        pageVo.setLimit(pageInfo.getPageSize());
        pageVo.setPages(pageInfo.getPages());
        pageVo.setTotal(pageInfo.getTotal());
        pageVo.setList(list);
        return pageVo;
    }
}
