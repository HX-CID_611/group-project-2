package com.cskaoyan.bean.vo;

import lombok.Data;

@Data
public class UserVo {
   String nickname;
   String avatar;
}
