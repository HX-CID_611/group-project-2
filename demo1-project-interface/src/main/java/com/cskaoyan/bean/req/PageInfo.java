package com.cskaoyan.bean.req;

import lombok.Data;
import org.apache.commons.beanutils.BeanUtils;

import java.lang.reflect.InvocationTargetException;
import java.util.Map;

@Data
public class PageInfo {
    private Integer page;
    private Integer limit;
    private String sort;
    private String order;


    // 给这四个参数赋值，然后计算offset，然后给offset赋值
    public static PageInfo transfer(Map<String, String[]> parameterMap) {
        PageInfo pageInfo = new PageInfo();
        try {
            BeanUtils.copyProperties(pageInfo, parameterMap);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        } catch (InvocationTargetException e) {
            throw new RuntimeException(e);
        }
        //Integer offset = (pageInfo.getPage() - 1) * pageInfo.getLimit();
        //pageInfo.setOffset(offset);
        return pageInfo;
    }
    public void init(Map<String, String[]> parameterMap) {
        try {
            BeanUtils.copyProperties(this, parameterMap);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        } catch (InvocationTargetException e) {
            throw new RuntimeException(e);
        }
    }
}
