package com.cskaoyan.bean.req;

import lombok.Data;

@Data
public class UserPageInfo extends PageInfo{
    String username;
    String mobile;
}
