package com.cskaoyan.bean.req;

import com.cskaoyan.bean.model.*;
import lombok.Data;

import java.util.List;

@Data
public class GoodCreateVo {
    MarketGoods goods;
    List<MarketGoodsAttribute> attributes;
    List<MarketGoodsProduct> products;
    List<MarketGoodsSpecification> specifications;
}
