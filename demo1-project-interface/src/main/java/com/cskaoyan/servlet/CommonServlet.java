package com.cskaoyan.servlet;


import com.cskaoyan.util.JsonUtil;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.lang.reflect.Method;

public class CommonServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            common(req, resp);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private void common(HttpServletRequest req, HttpServletResponse resp) throws Exception {
        // req.setCharacterEncoding("utf-8");
        // resp.setContentType("application/json;charset=utf-8");


        String uri = req.getRequestURI();
        String methodName = uri.substring(uri.lastIndexOf("/") + 1);

        Method method = this.getClass().getDeclaredMethod(methodName,HttpServletRequest.class,HttpServletResponse.class);
        method.setAccessible(true);
        Object result = method.invoke(this, new Object[]{req, resp});
        if (result != null) {

            resp.getWriter().println(JsonUtil.write(result));
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            common(req, resp);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
