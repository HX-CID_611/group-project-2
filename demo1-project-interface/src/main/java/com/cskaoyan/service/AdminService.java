package com.cskaoyan.service;

import com.cskaoyan.bean.model.MarketAdmin;

public interface AdminService {

    Integer login(String username, String password);
    MarketAdmin queryById(Integer id);
}
