package com.cskaoyan.service;

import com.cskaoyan.bean.model.MarketAdmin;
import com.cskaoyan.bean.model.MarketAdminExample;
import com.cskaoyan.mapper.MarketAdminMapper;
import com.cskaoyan.util.MyBatisUtil;
import org.apache.ibatis.session.SqlSession;

import java.util.List;

public class AdminServiceImpl implements AdminService {

    @Override
    public Integer login(String username, String password) {
        Integer userId = 0;
        SqlSession sqlSession = MyBatisUtil.getSqlSession();
        MarketAdminMapper adminMapper = sqlSession.getMapper(MarketAdminMapper.class);

        //MarketAdmin marketAdmin = adminMapper.selectByName(username);
        MarketAdminExample example = new MarketAdminExample();
        example.createCriteria().andUsernameEqualTo(username);
        List<MarketAdmin> marketAdmins = adminMapper.selectByExample(example);
        MarketAdmin marketAdmin = null;
        if (marketAdmins != null && marketAdmins.size() > 0) {
            marketAdmin = marketAdmins.get(0);
        }

        if (password.equals(marketAdmin.getPassword())) {
            userId = marketAdmin.getId();
        }

        sqlSession.close();
        return userId;
    }

    @Override
    public MarketAdmin queryById(Integer id) {
        SqlSession sqlSession = MyBatisUtil.getSqlSession();
        MarketAdminMapper adminMapper = sqlSession.getMapper(MarketAdminMapper.class);
        MarketAdmin marketAdmin = adminMapper.selectByPrimaryKey(id);
        sqlSession.close();
        return marketAdmin;
    }
}
