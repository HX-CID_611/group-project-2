package com.cskaoyan.typehandler;

import com.cskaoyan.util.JsonUtil;
import org.apache.ibatis.type.JdbcType;
import org.apache.ibatis.type.MappedJdbcTypes;
import org.apache.ibatis.type.MappedTypes;
import org.apache.ibatis.type.TypeHandler;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * 比如MarketGoods中的gallery这个成员变量的类型为String[]，在输入输出映射过程中会自动使用这个类型处理器
 * 1. 提供类型信息
 *      a. implements 实现接口的时候提供类型信息
 *      b. @MappedTypes、@MappedJdbcTypes
 * 2. 注册类型处理器 → mybatis配置文件中 typehandlers标签
 *     a. typehandler子标签 → 配置单个typehandler
 *     b. package子标签 → 批量配置
 */
@MappedTypes(String[].class) // bean类中的成员变量的类型
@MappedJdbcTypes(JdbcType.VARCHAR) // 数据库中的字段的类型
public class StringArrayTypeHandler implements TypeHandler<String[]> {
    // 输入映射 → 给预编译的sql语句提供参数
    // String[] parameter = marketGoods.getGallery();
    // String jsonStr = JsonUtil.write(parameter);
    // preparedStatement.setString(index,jsonStr)
    @Override
    public void setParameter(PreparedStatement ps, int index, String[] parameter, JdbcType jdbcType) throws SQLException {
        String jsonStr = JsonUtil.write(parameter);
        ps.setString(index, jsonStr);
    }
    // 输出映射过程 → 结果集的封装
    @Override
    public String[] getResult(ResultSet rs, String columnName) throws SQLException {
        String jsonStr = rs.getString(columnName); // 数组的json字符串
        String[] array = JsonUtil.read(jsonStr, String[].class);
        return array;
    }

    @Override
    public String[] getResult(ResultSet rs, int columnIndex) throws SQLException {
        String jsonStr = rs.getString(columnIndex); // 数组的json字符串
        String[] array = JsonUtil.read(jsonStr, String[].class);
        return array;
    }

    @Override
    public String[] getResult(CallableStatement cs, int columnIndex) throws SQLException {
        String jsonStr = cs.getString(columnIndex); // 数组的json字符串
        String[] array = JsonUtil.read(jsonStr, String[].class);
        return array;
    }
}
